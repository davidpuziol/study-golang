# Study GOlang

<img src="./pics/golang.png" alt="drawing" width="150"/>

<img src="./pics/gopher.png" alt="drawing" width="250"/>

A linguagem golang é [opensource](https://github.com/golang/go), criada pelo Google, e tem o objetivo ser uma linguagem simples e produtiva.

- É uma linguagem expressiva, concisa, limpa (pouco verbosa) e eficiente (resolve muita coisa com pouco esforço)
- Foi pensada para aproveita ao máximo os recursos multicore (Perfeita para usar paralelismo) e rede
- É uma linguagem atual
- É uma linguagem rápida por ser compilada
- É estaticamente tipada
- Possue um garbage collector eficiente
- É uma linguagem de uso geral usada por várias empresas como Hashicorp, Meta, Netflix, Uber, etc.
- Gera tudo em `apenas um arquivo binário` executável (com tudo dentro) que deve ser compilado para o sistema operacional desejado, tornando o deploy extremamente simples.
- Enquanto estiver na versão 1.XX é totalmente retrocompatível dando muita segurança pensando em longo prazo.
- Frameworks de testes e profiling built in
- Detecta condição de corrida
- `Baixa curva de aprendizado`

>O compilador da linguagem é escrito na própria linguagem (em Golang)

Apesar de GO ser uma linguagem de uso geral, devemos entender cada linguagem tem suas vantagens e desvantagens e não ser cego para olhar para o lado. O fato de GO ser simples faz com que algo seja feito de uma única maneira sem muitas formas de fazer. Isso não desmerece a linguagem mas não tem tantos recursos redundantes.

**Na carreira DevOps, entender GO poderá te ajudar muito a compreender o código fonte de muitos dos binários que você utiliza no dia a dia.**

[Site Go](https://go.dev/)
[Site Go](https://go.dev/)

## Como estudar?

Talvez seja necessário um pouco de estudo extra sobre alguns termos usados, alguns fundamentos. Estou levando em consideração que não seja o seu primeiro contato com uma linguagem de programação e somente precisa entender os limítes da linguagem.

Siga o passo a passo dos manuscritos:

[Preparando o Ambiente](./manuscritos/01-ambiente.md) - Instalação do GO e Dicas de extensão no uso do VSCode.
[Entendendo pacotes](./manuscritos/02-fundamentos/02.01-pacotes.md) - Um overview sobre como o go usa os pacotes e módulos
[Variáveis](./manuscritos/02-fundamentos/02.02-variaveis.md) - Sobre como usar as variáveis em go
[Lógica](./manuscritos/02-fundamentos/02.03-logica.md) - Quais as condicionais usadas e como usar
[Funcões](./manuscritos/02-fundamentos/02.04-funcoes.md) - Como o go trabalha com funções
[Preparando o Ambiente](./manuscritos/02-fundamentos/02.05-compilando.md) - Como funciona o processo de compilação no golang
[Pacotes Mais Usados](./manuscritos/02-fundamentos/02.05-compilando.md) - Como funciona o processo de compilação no golang