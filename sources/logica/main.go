package main

import "fmt"

func main() {
	var myArray [3]int

	myArray[0] = 1
	myArray[1] = 2
	myArray[2] = 3
	println("Primeiro exemplo de for")
	//
	for i := 0; i < len(myArray)-1; i++ {
		fmt.Printf("O valor o indice %d é %d \n", i, myArray[i])
	}

	println("Segundo exemplo de for")

	//Definndo um array já com os valores sendo que ele esta usando inferência
	myArray2 := [3]int{10, 20, 30}
	// i será o indice e o v é valor no myarray no indice i
	for i, v := range myArray2 {
		fmt.Printf("O valor o indice %d é %d \n", i, v)
	}

	// Nesse caso vc esta dizendo que um array é igual a outro array, é uma forma mais verbosa
	var myArray3 [3]int = [3]int{100, 200, 300}

	println("Terceiro exemplo de for atuando como while")
	i := 0 // Inicializando i
	for i < len(myArray3) {
		fmt.Printf("O valor o indice %d é %d \n", i, myArray3[i])
		i++ // incrementa
	}

	myMap := map[string]int{"banana": 1, "maca": 3, "pera": 5}
	for nome, quantidade := range myMap {
		fmt.Printf("%s tem %d\n", nome, quantidade)
	}
	// Se quisesse ignorar o nome poderia só colocar um underline
	for _, quantidade := range myMap {
		fmt.Printf("%d\n", quantidade)
	}

	//observe que o range cria o key e values de cada uma das posições
	teste := []string{"um", "dois", "tres"}
	for k, v := range teste {
		println(k, v)
	}

	a := "a"
	b := "b"
	c := "c"
	switch a {
	case "a":
		println(a)
	case "b":
		println(b)
	case "c":
		println(c)
	default:
		println("d")
	}

	// for {
	// 	println("Estou em um loop infinito")
	// }
}
