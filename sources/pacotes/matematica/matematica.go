package matematica

type Number interface {
	int | float64
}

var A int = 10 // exportada
var b int = 20 // não exportada

// Estamos usando slice aqui

func Mult[T Number](s []T) T {
	var mult T = 1
	for _, param := range s {
		mult = mult * param
	}
	return mult
}

func Sum[T Number](s []T) T {
	var mult T = 0
	for _, param := range s {
		mult += param
	}
	return mult
}
