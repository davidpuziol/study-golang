package main

import (
	"fmt"
	"pacotes/matematica"
	"pacotes/matematica/matematicaTeste"
)

func main() {
	fmt.Println(matematica.Sum([]int{1, 2, 3, 4, 5, 6, 7}))
	fmt.Printf("%.2f\n", matematica.Mult([]float64{1, 2, 3, 4, 5.9, 6.4, 7}))
	matematicaTeste.Teste()
}
