package main

import (
	"errors"
	"fmt"
)

func main() {
	oi()
	soma0(0, 2)
	fmt.Println(soma1(1, 3))
	fmt.Println(soma2(2, 4))
	fmt.Println(soma3(5, 10)) // observe que ele irá imprimir tb o bool
	valor, err := soma4(1, 50)
	//valor:= soma4(1, 50) isso seria um erro, pois a funcao volta dois valores
	if err != nil { // Se for diferente de nil significa que o valor é maior que 50
		fmt.Println(err)
	} else {
		fmt.Println(valor)
	}

	fmt.Println(soma5(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 50))

	// Isso é uma clousure e esta função é interna a outra função
	total := func() int {
		return soma5(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 50) * 2
	}()
	fmt.Println(total)

	println(MultInt(map[string]int{"a": 5, "b": 10, "c": 20, "d": 30}))
	fmt.Printf("%.2f\n", MultFloat(map[string]float64{"a": 5.0, "b": 10.2, "c": 2.0, "d": 1.3}))

	println(Mult(map[string]int{"a": 5, "b": 10, "c": 20, "d": 30}))
	fmt.Printf("%.2f\n", Mult(map[string]float64{"a": 5.0, "b": 10.2, "c": 2.0, "d": 1.3}))
}

// funcao que nao recebe nada na entrada e nao tem nada na saída
func oi() {
	println("hello")
}

// funcao que recebe valores de entrada, mas nao tem saída
func soma0(a int, b int) {
	fmt.Println(a + b)
}

// funcao que recebe valores de entrada, e tem um valor de saída
func soma1(a int, b int) int {
	return a + b
}

//outro modo de declarar sendo que a e b tem o mesmo tipo
func soma2(a, b int) int {
	return a + b
}

//uma função pode retornar mais de um valor. Isso é muito utilizado para erros, pois o GO não tem exceptions
// função com varias entradas e varias saídas
func soma3(a, b int) (int, bool) {
	if a+b >= 50 {
		return a + b, true
	}
	return a + b, false
}

// usando com erros e o pessoal costuma usar no final sendo necessario importar a lib
func soma4(a, b int) (int, error) {
	if a+b >= 50 {
		return 0, errors.New("A soma é maior que 50")
	}
	return a + b, nil
}

// Essa é uma funcao variática o número será um array do mesmo tipo
func soma5(numeros ...int) int {
	soma := 0
	for _, param := range numeros {
		// for index, param := range numeros { // o primeiro valor seria o index
		soma += param
		// println(index)
	}
	return soma
}

// Essa função receberia um parametro Map do tipo "string": int, "string": int e multiplicaria todo mundo
func MultInt(m map[string]int) int {
	mult := 1
	for _, param := range m {
		mult *= param
	}
	return mult
}

// Essa seria uma segunda função para receber float
func MultFloat(m map[string]float64) float64 {
	var mult float64 = 1
	for _, param := range m {
		mult = mult * param
	}
	return mult
}

// Essa seria a função genérica para as duas acima
// func Mult[T int | float64](m map[string]T) T {
// 	var mult T = 1
// 	for _, param := range m {
// 		println(param)
// 		mult = mult * param
// 	}
// 	return mult
// }

type Number interface {
	int | float64
}

func Mult[T Number](m map[string]T) T {
	var mult T = 1
	for _, param := range m {
		mult = mult * param
	}
	return mult
}
