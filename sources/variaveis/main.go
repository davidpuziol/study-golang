package main

// Essas variáveis são de escopo global e já observe que o comentário é feito com duas barras para uma linha
import "fmt"

/*
isso é um comentário de um bloco inteiro abrinco com /* e fechando com
*/

type ID int

//Essa Interface vazia será uma simulação de  um object, ou seja, todo mundo implementa ela
type Object interface{}

//Aceita qualquer coisa, mas a chance de ter erro é muito alta
func ShowType(t Object) {
	fmt.Printf("O tipo da variável é %T e o valor é %v\n", t, t)
}

type Pessoa interface {
	Desativar()
}

//
func Disable(pessoa Pessoa) {
	pessoa.Desativar() //Esse método esta tando presente no cliente quanto na empresa
}

type Endereco struct {
	Logradouro string
	Numero     int
	Cidade     string
	Estado     string
}

type Cliente struct {
	Nome     string
	Idade    int
	Ativo    bool
	Salario  float32
	Endereco //Isso é uma composição nesse caso não preciso passar o tipo
	// Endereco Endereco // Isso não daria erro, mas não é uma composição
}

//Método desativar do cliente
func (p *Cliente) Desativar() {
	p.Ativo = false
}

type Empresa struct {
	Nome     string
	Ativo    bool
	Endereco //Isso é uma composição nesse caso não preciso passar o tipo
	// Endereco Endereco // Isso não daria erro, mas não é uma composição
}

//Método desativar da empresa
func (e *Empresa) Desativar() {
	e.Ativo = false
}

const a = "Hello, World"
const z = 11

// posso declarar assim qualquer variável
var b bool
var c int

// ou posso declarar um conjunto para diminuir a verbosidade
var (
	d string
	e float64
	f bool = true
	g int
	h int64
	i float64 = 1.6
	j string  = "David"
	t ID      = 1
)

func main() {

	println(a) // neste momento ele vai imprimir a constante
	println(b)
	println(c)
	println(d)
	println(e)
	println(f)
	println(g)
	println(h)
	println(i)
	println(j)
	var idade int = 38
	println(idade) // faça um teste comentando esta linha e para ver que se idade for declarada mas não utilizada no escopo local, o que acontece.
	var a string = "X"
	println(a) // neste momento ele vai imprimir X ao invés da constante
	// podemos também declarar uma variavel de forma menos verbosa
	k := 11 // ele sabe que é um int
	println(k)
	l := "11" // ele sabe que é uma string
	println(l)
	println(t)

	//Para printar algo formatando usamos o pacote fmt, \n pula uma linha
	fmt.Printf("O tipo de t é %T \n", t)   //tipo
	fmt.Printf("O o valor de t é %v\n", t) //valor

	var myArray [3]int
	println(myArray[0])

	//Arrays possuem tamanho fixo
	myArray[0] = 1
	myArray[1] = 2
	myArray[2] = 3
	println(myArray[0])
	println(myArray[1])
	println(myArray[2])
	//println(myArray[3]) // Isso daria erro pois esta fora do escopo

	println("Usando um slice")
	mySlice := []int{2, 4, 6, 8, 10} //o fato de não ter um tamanho infere que será um slice
	fmt.Printf("len=%d cap=%d %v\n", len(mySlice), cap(mySlice), mySlice)

	println("Usando um Map")
	myMap := map[string]int{"banana": 1, "maca": 3, "pera": 5}
	//se fosse inicializar vazio myMap := map[string]int{}
	delete(myMap, "pera")
	myMap["uva"] = 7 //vai adicionar no final, pois map nao é ordenado, vc busca pela chave
	myMap["maca"] = 4
	fmt.Println(myMap)

	cliente1 := Cliente{
		Nome:    "David",
		Idade:   38,
		Ativo:   true,
		Salario: 20.5,
	}
	cliente1.Cidade = "Vilha Velha"
	cliente1.Desativar() //Usando o método vinculado a para colocar ativo em false
	// cliente1.Endereco.Cidade = "Vilha Velha" Também funcionaria
	fmt.Printf("%s %d Idade Ativo: %t e recebe %f\n", cliente1.Nome, cliente1.Idade, cliente1.Ativo, cliente1.Salario)
	empresa1 := Empresa{
		Nome:  "Home",
		Ativo: true,
	}

	Disable(&empresa1) //observe que precisou passar o ponteiro
	fmt.Printf("%s Ativo: %t\n", empresa1.Nome, empresa1.Ativo)

	teste := 10
	var ponteiro *int = &teste //o ponteiro aponta para o endereço de memória de a
	fmt.Printf("o valor do ponteiro é %v\n", ponteiro)
	fmt.Printf("o conteúdo do ponteiro é %v\n", *ponteiro)

	var obj1 Object = 10
	var obj2 Object = "Opsss"
	ShowType(obj1)
	ShowType(obj2)

	println(obj1)
	println(obj1.(int))            // um cast quando tem certeza funciona
	resultado, ok := obj1.(string) //estamos tentando converter para algo que ela não é
	println(resultado)             //o valor
	println(ok)                    //se deu certo true ou false
	/*
		resultado2 := obj2.(int) //um cast errado causaria panic na aplicacao pois o obj2 é uma string
		println(resultado2) //o valor
	*/
}
