# Cobra

![Alt text](cobra.png)

O go é muito utilizado por várias tecnologias para fazer command line interfaces. O principal pacote utilizado é o [cobra](https://github.com/spf13/cobra).

Se você é DevOps pode ter certeza que essa será a primeira coisa que gostaria de saber em go. Aqui uma lista dos principais projetos usando go <https://github.com/spf13/cobra/blob/main/site/content/projects_using_cobra.md> e pode observar que o helm, kubernetes, docker, istio, e muitos outros projetos usam o cobra.
