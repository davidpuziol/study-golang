# Ambiente

## Como instalar o compilador GO?

Segundo o link <https://go.dev/dl/> poderá fazer o download do compilador direto na sua máquina e executar.

No linux podemos baixar fonte e colocar em algumas pasta que esteja no path do sistema como por exemplo /usr/local/bin ou simplesmente instalar pelo gerenciado de pacotes (como eu mesmo faço) que é muito mais fácil.

```bash
# No ubuntu
sudo apt-get install golang
# Se o comando abaixo funcionar significa que esta encontrando o compilador
go version
#go version go1.18.1 linux/amd64
```

Não vale a pena ficar explicando a instalação se está na documentação para qualquer sistema. Siga o link caso esteja no mac ou windows.

Pelo homebrew seria simplesmente `brew install go`

## Configurando o GO

Se digitarmos o comando `go env` vamos ver a seguinte saída (Na própria saída eu comentei algumas coisas aqui). Essas variáveis definem o comportament do GO conforme você for programar.

```bash
go env
GO111MODULE=""
GOARCH="amd64"
GOBIN=""
GOCACHE="/home/david-prata/.cache/go-build"
GOENV="/home/david-prata/.config/go/env"
GOEXE=""
GOEXPERIMENT=""
GOFLAGS=""
GOHOSTARCH="amd64"
GOHOSTOS="linux"
GOINSECURE=""
# Gerenciamento de dependencias no go e geralmente esta na pasta pkg dentro do GOPATH
GOMODCACHE="/home/david-prata/go/pkg/mod"
GONOPROXY=""
GONOSUMDB=""
GOOS="linux"
# Essa será a pasta na qual o go vai interpretar os arquivos, onde irá instalar pacotes de terceiros, onde irá gerar os seus binários.
GOPATH="/home/david-prata/go"
GOPRIVATE=""
GOPROXY="https://proxy.golang.org,direct"
GOROOT="/usr/lib/go-1.18"
GOSUMDB="sum.golang.org"
GOTMPDIR=""
GOTOOLDIR="/usr/lib/go-1.18/pkg/tool/linux_amd64"
GOVCS=""
GOVERSION="go1.18.1"
GCCGO="gccgo"
GOAMD64="v1"
AR="ar"
CC="gcc"
CXX="g++"
CGO_ENABLED="1"
GOMOD="/dev/null"
GOWORK=""
CGO_CFLAGS="-g -O2"
CGO_CPPFLAGS=""
CGO_CXXFLAGS="-g -O2"
CGO_FFLAGS="-g -O2"
CGO_LDFLAGS="-g -O2"
PKG_CONFIG="pkg-config"
GOGCCFLAGS="-fPIC -m64 -pthread -fmessage-length=0 -fdebug-prefix-map=/tmp/go-build2662800121=/tmp/go-build -gno-record-gcc-switches"
```

Para garantir que esta tudo funcionando, vamos fazer nosso [HelloWorld](../sources/helloworld.go). Consulte o código e veja a simplicidade.

```go
go run sources/helloworld/main.go 
HelloWorld
```

## VSCode

Antes de continuar dizem que a melhor IDE para trabalhar com GO é a [Goland](https://www.jetbrains.com/go/) da Jetbrains, mas é paga. Eu gosto do VSCode, porém com as extensões corretas.

Então vamos configurar o VSCode.

Instale a extensão do GO do vscode do próprio time da google.

![Extension](../pics/golangextension.png)

Porém precisamos configurar essa extensão

No vscode apertando ctrl shift P temos a barra de comando e faça como no gif abaixo

![Extension](../pics/configvscode.gif)
